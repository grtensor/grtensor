=================================
GRTensorIII for Maple
=================================

This project has moved to GITHUB:
https://github.com/grtensor/grtensor

Why?
GITLAB does not collect download statistics. These are useful to 
validate continued maintenence of GRTensorIII.
